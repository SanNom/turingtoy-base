#!./.venv/bin/python3

from src.turingtoy import (
    run_turing_machine,
)
from typing import (
    Any,
    Dict,
    List,
)

from tests.test_turing import to_dict

print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Test 1-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")

def simple_test() -> None:
    machine = {
        "blank": "0",
        "start state": "e1",
        "final states": ["done"],
        "table": {
            "e1": {
                "0": {"L": "done"},
                "1": {"write": "0", "R": "e2"},
            },
            "e2": {
                "1": {"write": "1", "R": "e2"},
                "0": {"write": "0", "R": "e3"},
            },
            "e3": {
                "1": {"write": "1", "R": "e3"},
                "0": {"write": "1", "L": "e4"},
            },
            "e4": {
                "1": {"write": "1", "L": "e4"},
                "0": {"write": "0", "L": "e5"},
            },
            "e5": {
                "1": {"write": "1", "L": "e5"},
                "0": {"write": "1", "R": "e1"},
            },
            "done": {},
        },
    }

    input_ = "111"
    output, execution_history, accepted = run_turing_machine(machine, input_)
    print("----------------------")
    print("Output : " + str(output))
    print("History : " + str(execution_history))
    print("final_step_reached : " + str(accepted))
    
    expected = [    {      "state": "e1",      "reading": "1",      "position": 0,      "memory": "111",      "transition": {        "write": "0",        "R": "e2"      }    },    {      "state": "e2",      "reading": "1",      "position": 1,      "memory": "011",      "transition": {        "write": "1",        "R": "e2"      }    },    {      "state": "e2",      "reading": "1",      "position": 2,      "memory": "011",      "transition": {        "write": "1",        "R": "e2"      }    },    {      "state": "e2",      "reading": "0",      "position": 3,      "memory": "0110",      "transition": {        "write": "0",        "R": "e3"      }    },    {      "state": "e3",      "reading": "0",      "position": 4,      "memory": "01100",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "0",      "position": 3,      "memory": "01101",      "transition": {        "write": "0",        "L": "e5"      }    },    {      "state": "e5",      "reading": "1",      "position": 2,      "memory": "01101",      "transition": {        "write": "1",        "L": "e5"      }    },    {      "state": "e5",      "reading": "1",      "position": 1,      "memory": "01101",      "transition": {        "write": "1",        "L": "e5"      }    },    {      "state": "e5",      "reading": "0",      "position": 0,      "memory": "01101",      "transition": {        "write": "1",        "R": "e1"      }    },    {      "state": "e1",      "reading": "1",      "position": 1,      "memory": "11101",      "transition": {        "write": "0",        "R": "e2"      }    },    {      "state": "e2",      "reading": "1",      "position": 2,      "memory": "10101",      "transition": {        "write": "1",        "R": "e2"      }    },    {      "state": "e2",      "reading": "0",      "position": 3,      "memory": "10101",      "transition": {        "write": "0",        "R": "e3"      }    },    {      "state": "e3",      "reading": "1",      "position": 4,      "memory": "10101",      "transition": {        "write": "1",        "R": "e3"      }    },    {      "state": "e3",      "reading": "0",      "position": 5,      "memory": "101010",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "1",      "position": 4,      "memory": "101011",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "0",      "position": 3,      "memory": "101011",      "transition": {        "write": "0",        "L": "e5"      }    },    {      "state": "e5",      "reading": "1",      "position": 2,      "memory": "101011",      "transition": {        "write": "1",        "L": "e5"      }    },    {      "state": "e5",      "reading": "0",      "position": 1,      "memory": "101011",      "transition": {        "write": "1",        "R": "e1"      }    },    {      "state": "e1",      "reading": "1",      "position": 2,      "memory": "111011",      "transition": {        "write": "0",        "R": "e2"      }    },    {      "state": "e2",      "reading": "0",      "position": 3,      "memory": "110011",      "transition": {        "write": "0",        "R": "e3"      }    },    {      "state": "e3",      "reading": "1",      "position": 4,      "memory": "110011",      "transition": {        "write": "1",        "R": "e3"      }    },    {      "state": "e3",      "reading": "1",      "position": 5,      "memory": "110011",      "transition": {        "write": "1",        "R": "e3"      }    },    {      "state": "e3",      "reading": "0",      "position": 6,      "memory": "1100110",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "1",      "position": 5,      "memory": "1100111",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "1",      "position": 4,      "memory": "1100111",      "transition": {        "write": "1",        "L": "e4"      }    },    {      "state": "e4",      "reading": "0",      "position": 3,      "memory": "1100111",      "transition": {        "write": "0",        "L": "e5"      }    },    {      "state": "e5",      "reading": "0",      "position": 2,      "memory": "1100111",      "transition": {        "write": "1",        "R": "e1"      }    },    {      "state": "e1",      "reading": "0",      "position": 3,      "memory": "1110111",      "transition": {        "L": "done"      }    }  ]
    for i in range(len(execution_history)):
        if execution_history[i] != expected[i]:
            print("Different : " + str(i))
simple_test()
'''
print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Test 2-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")


def test2() -> None:
    machine = {
        "blank": " ",
        "start state": "right",
        "final states": ["done"],
        "table": {
            # Start at the second number's rightmost digit.
            "right": {
                **to_dict(["0", "1", "+"], "R"),
                " ": {"L": "read"},
            },
            # Add each digit from right to left:
            # read the current digit of the second number,
            "read": {
                "0": {"write": "c", "L": "have0"},
                "1": {"write": "c", "L": "have1"},
                "+": {"write": " ", "L": "rewrite"},
            },
            # and add it to the next place of the first number,
            # marking the place (using O or I) as already added.
            "have0": {**to_dict(["0", "1"], "L"), "+": {"L": "add0"}},
            "have1": {**to_dict(["0", "1"], "L"), "+": {"L": "add1"}},
            "add0": {
                **to_dict(["0", " "], {"write": "O", "R": "back0"}),
                "1": {"write": "I", "R": "back0"},
                **to_dict(["O", "I"], "L"),
            },
            "add1": {
                **to_dict(["0", " "], {"write": "I", "R": "back1"}),
                "1": {"write": "O", "L": "carry"},
                **to_dict(["O", "I"], "L"),
            },
            "carry": {
                **to_dict(["0", " "], {"write": "1", "R": "back1"}),
                "1": {"write": "0", "L": "carry"},
            },
            # Then, restore the current digit, and repeat with the next digit.
            "back0": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "0", "L": "read"},
            },
            "back1": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "1", "L": "read"},
            },
            # Finish: rewrite place markers back to 0s and 1s.
            "rewrite": {
                "O": {"write": "0", "L": "rewrite"},
                "I": {"write": "1", "L": "rewrite"},
                **to_dict(["0", "1"], "L"),
                " ": {"R": "done"},
            },
            "done": {},
        },
    }

    input_ = "11+1"
    output, execution_history, accepted = run_turing_machine(machine, input_)
    print("----------------------")
    print("Output : " + str(output))
    print("History : " + str(execution_history))
    print("final_step_reached : " + str(accepted))
test2()

print("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*Test 3-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*")

def test3() -> None:
    machine = {
        "blank": " ",
        "start state": "start",
        "final states": ["done"],
        "table": {
            # Prefix the input with a '+', and go to the rightmost digit.
            "start": {
                **to_dict(["0", "1"], {"L": "init"}),
            },
            "init": {
                " ": {"write": "+", "R": "right"},
            },
            "right": {
                **to_dict(["0", "1", "*"], "R"),
                " ": {"L": "readB"},
            },
            # Read and erase the last digit of the multiplier.
            # If it's 1, add the current multiplicand.
            # In any case, double the multiplicand afterwards.
            "readB": {
                "0": {"write": " ", "L": "doubleL"},
                "1": {"write": " ", "L": "addA"},
            },
            "addA": {
                **to_dict(["0", "1"], "L"),
                "*": {"L": "read"},  # enter adder
            },
            # Double the multiplicand by appending a 0.
            "doubleL": {
                **to_dict(["0", "1"], "L"),
                "*": {"write": "0", "R": "shift"},
            },
            "double": {  # return from adder
                **to_dict(["0", "1", "+"], "R"),
                "*": {"write": "0", "R": "shift"},
            },
            # Make room by shifting the multiplier right 1 cell.
            "shift": {
                "0": {"write": "*", "R": "shift0"},
                "1": {"write": "*", "R": "shift1"},
                " ": {"L": "tidy"},  # base case: multiplier = 0
            },
            "shift0": {
                "0": {"R": "shift0"},
                "1": {"write": "0", "R": "shift1"},
                " ": {"write": "0", "R": "right"},
            },
            "shift1": {
                "0": {"write": "1", "R": "shift0"},
                "1": {"R": "shift1"},
                " ": {"write": "1", "R": "right"},
            },
            "tidy": {
                **to_dict(["0", "1"], {"write": " ", "L": "tidy"}),
                "+": {"write": " ", "L": "done"},
            },
            "done": {},
            # This is the 'binary addition' machine almost verbatim.
            # It's adjusted to keep the '+'
            # and to lead to another state instead of halting.
            "read": {
                "0": {"write": "c", "L": "have0"},
                "1": {"write": "c", "L": "have1"},
                "+": {"L": "rewrite"},  # keep the +
            },
            "have0": {**to_dict(["0", "1"], "L"), "+": {"L": "add0"}},
            "have1": {**to_dict(["0", "1"], "L"), "+": {"L": "add1"}},
            "add0": {
                **to_dict(["0", " "], {"write": "O", "R": "back0"}),
                "1": {"write": "I", "R": "back0"},
                **to_dict(["O", "I"], "L"),
            },
            "add1": {
                **to_dict(["0", " "], {"write": "I", "R": "back1"}),
                "1": {"write": "O", "L": "carry"},
                **to_dict(["O", "I"], "L"),
            },
            "carry": {
                **to_dict(["0", " "], {"write": "1", "R": "back1"}),
                "1": {"write": "0", "L": "carry"},
            },
            "back0": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "0", "L": "read"},
            },
            "back1": {
                **to_dict(["0", "1", "O", "I", "+"], "R"),
                "c": {"write": "1", "L": "read"},
            },
            "rewrite": {
                "O": {"write": "0", "L": "rewrite"},
                "I": {"write": "1", "L": "rewrite"},
                **to_dict(["0", "1"], "L"),
                " ": {"R": "double"},  # when done, go to the 'double' state
            },
        },
    }

    input_ = "11*101"
    output, execution_history, accepted = run_turing_machine(machine, input_)
    print("----------------------")
    print("Output : " + str(output))
    print("History : " + str(execution_history))
    print("final_step_reached : " + str(accepted))
test3()'''
from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)
import logging, sys
import poetry_version
LOG_LEVEL = logging.ERROR

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:
    print(machine)

    turing_machine = TuringMachine(machine, input_, steps)

    output, history, can_reach_final_step = turing_machine.execute()
    print("output : " + output)
    print("history : " + str(history))
    print("can reach final step : " + str(can_reach_final_step))
    return output, history, can_reach_final_step

def init_logs():
    logger = logging.getLogger()
    logger.setLevel(LOG_LEVEL)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(LOG_LEVEL)
    formatter = logging.Formatter('%(levelname)s -> %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    return logger

class TuringMachine:
    def __init__(self, machine: Dict, input_: str, steps: Optional[int] = None):
        self.logger = init_logs()

        self.machine = machine
        self.input_ = input_    # La bande a ne pas toucher
        if steps is not None:
            self.max_steps = steps
        else:
            self.max_steps = -1

        self.blank = self.machine.get("blank")
        self.start_state = self.machine.get("start state")
        self.final_states = self.machine.get("final states")
        self.table = self.machine.get("table")

        self.logger.debug(f"Blank : {self.blank}")
        self.logger.debug(f"Start state : {self.start_state}")
        self.logger.debug(f"Final states : {self.final_states}")
        self.logger.debug(f"Max steps : {self.max_steps}")

    def execute(self):
        self.output = self.input_  # La bande à modifier
        self.pos = 0
        next_pos = self.pos
        act_count_step = 0
        self.can_reach_final_step = True
        history = []
        # Commence par la start state
        act_state_name = None
        next_state_name = self.start_state

        # Tant que pas max_steps | bloqué | final_state
        while (self.max_steps == -1 or self.max_steps > self.act_count_step) and next_state_name is not None not in self.final_states and self.can_reach_final_step:
            #history.append(self.backup_history(state=next_state_name, reading=, position=self.pos, memory=self.output, transition=self.state.get("")))
            previous_output = self.output   # For history
            self.pos = next_pos
            act_state_name = next_state_name
            # Execute instruction
            next_state_name, next_pos = self.compute_instruction(act_state_name)

            # Ajoute l'historique
            if (self.max_steps == -1 or self.max_steps > self.act_count_step) and next_state_name is not None not in self.final_states and self.can_reach_final_step:
                history.append(self.backup_history(previous_output, act_state_name, next_state_name))

            self.previous_transition = self.transition
            act_count_step += 1

        if next_state_name in self.final_states or act_state_name in self.final_states :
            self.can_reach_final_step = True

        assert type(self.remove_trailing_blank(self.output)) is str
        assert type(history) is list
        assert type(self.can_reach_final_step) is bool
        return self.remove_trailing_blank(self.output), history, self.can_reach_final_step


    def compute_instruction(self, state_name: str) -> Tuple[str, int]:
        next_state_name = None
        next_pos = None
        self.logger.info(f"Instruction : {state_name}")
        state = self.table.get(state_name)
        if self.pos >= len(self.output):
            self.output = self.output + self.blank
        elif self.pos < 0:
            self.output = self.blank + self.output
            self.pos += 1
        band_carac = self.output[self.pos]
        action = state.get(band_carac)  # action = {action, movement: next_i}
        if type(action) is dict:
            self.transition = action    # For history
        else:
            self.transition = state
        self.reading = band_carac

        self.logger.debug("Found action " + str(action))
        if action is None:
            self.logger.debug("locked")
            self.can_reach_final_step = False
        else:
            self.logger.debug("working")
            # Action
            if "write" in action:
                self.logger.debug(f"Writing {action.get('write')}")
                self.output = self.output[:self.pos] + action.get("write") + self.output[self.pos+1:]

            # Movement
            if "R" in action:
                self.logger.debug("Going right")
                next_pos = self.pos+1
                if type(action) is dict:
                    next_state_name = action.get("R")
            elif "L" in action:
                self.logger.debug("Going left")
                next_pos = self.pos-1
                if type(action) is dict:
                    next_state_name = action.get("L")

        if next_state_name is None:
            next_state_name = state_name
            self.logger.debug("No next state set, rewriting")
        if not self.transition:
            self.transition = self.previous_transition
        if self.transition is None:
            self.transition = next_state_name
        if next_pos is None:
            next_pos = self.pos

        if next_pos >= len(self.output):
            self.output = self.output + self.blank
        elif next_pos < 0:
            self.output = self.blank + self.output
            next_pos += 1
        return next_state_name, next_pos


    def backup_history(self, previous_output, act_state_name, next_state_name) -> dict:
        reading = self.output[self.pos-1]
        pos = self.pos
        memory = previous_output
        act_history = None
        act_history = {"state": act_state_name, "reading": self.reading, "position": pos, "memory": memory, "transition": self.transition}

        return act_history

    def remove_trailing_blank(self, string, get_correct_position=False):
        correct_pos = self.pos+1    # +1 ?
        while string[-1] == self.blank:
            string = string[:-1]
            correct_pos += 1
        while string[0] == self.blank:
            string = string[1:]
            correct_pos -= 1

        if get_correct_position:
            return string, correct_pos
        else:
            return string


"""
Params : 
machine : Dict d'input (code)
input_ : Chaine contenu de la bande en début d'execution
steps(optionnel) : nbr max de transitions a effectuer

Historique d'execution :
[{state, reading, position, memory, transition}]

Retour :
(état bande en sortie, historique d'execution, non_bloqué==true)
"""
